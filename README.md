# Pin Puller

An Open Hardware Pin Puller for space applications

## Contents
Concepts folder contains conceptual crude designs of mechanisms. These designs serve the purpose of verifying different approaches using simple construction methods and 3D printing

## License
Released under CERN-OHL-S, see [LICENSE](LICENSE) file for details
